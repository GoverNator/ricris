#pragma once

#ifndef _COMMON_DATA_HPP_
#define _COMMON_DATA_HPP_

const char *	DEFAULT_PORT = "27015";			// TODO choose another port
const int		QUEUE_SIZE = 5;

const wchar_t* SERVER_NAME = L"This is Remote Control Server";

#endif // _COMMON_DATA_HPP_
