﻿#include "stdafx.h"

#include "Socket.hpp"

#include <windows.h>

#pragma comment (lib, "Network_x86.lib")

const int width = 65535;
const int height = 65535;

struct EventType
{
	enum Enum : short
	{
			Mouse
		,	Keyboard
	};
};

INPUT parseMouse(std::stringstream & _eventStream)
{
	INPUT input;
	input.type = INPUT_MOUSE;

	_eventStream
		>> input.mi.dx
		>> input.mi.dy
		>> input.mi.mouseData
		>> input.mi.dwFlags
		;

	input.mi.time = 0;
	input.mi.dwExtraInfo = 0;

	return input;
}

INPUT parseKeyboard(std::stringstream & _eventStream)
{
	INPUT input;
	input.type = INPUT_KEYBOARD;

	_eventStream
		>> input.ki.wVk
		>> input.ki.wScan
		>> input.ki.dwFlags;

	input.ki.time = 0;
	input.ki.dwExtraInfo = 0;

	return input;
}

INPUT parseEvent(const std::string & _event)
{
	std::stringstream eventStream(_event);
	
	short kind = 3;

	eventStream >> kind;

	switch (kind)
	{
	case EventType::Mouse:
		return parseMouse(eventStream);

	case EventType::Keyboard:
		return parseKeyboard(eventStream);

	default:
		throw std::runtime_error("unknown event type");
	}

	return INPUT();
}

int main(void)
{
	while (true)
	{
		Socket server(Socket::Kind::SERVER);

		try
		{
			server.runServer();
			server.getServerIP();

			server.acceptClient();

			while (true)
			{
				auto input = parseEvent(server.read());
				//std::string eventCode, x, y;

				//x = server.read();
				//y = server.read();

				//INPUT input;
				//input.type = INPUT_MOUSE;

				//input.mi.dx = std::atof(x.c_str());
				//input.mi.dy = std::atof(y.c_str());
				//input.mi.dwFlags = MOUSEEVENTF_VIRTUALDESK | MOUSEEVENTF_MOVE;
				//input.mi.dwExtraInfo = 0;
				//input.mi.mouseData = DWORD();
				//input.mi.time = 0;

				SendInput(1, &input, sizeof(input));
			}
		}
		catch (const std::exception&)
		{
			server.reset();
			system("pause");
		}
	}

	return 0;
}


