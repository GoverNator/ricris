#pragma once

#ifndef _SOCKET_HPP_
#define _SOCKET_HPP_

#include <string>
#include <winsock2.h>

class Socket
{
public:
	enum class Kind
	{
			CLIENT
		,	SERVER 
	};

public:
					Socket(Kind _kind);
					~Socket();

	void			runServer(const char* _port = "default");
	void			runClient(const char* _ip, const char* _port = "default");

	void			acceptClient();

	void			reset();

	std::string		read();
	void			write(const std::string & _data);

	void			getServerIP();

//	void			close();

private:
	static void		initialize();

	void			fillAddrInfo(
							const char * _ip
						,	const char * _port
						,	addrinfo ** _addrInfo
						,	addrinfo & _hints
					);
	void			prepareSocket(addrinfo * _addrinfo);

private:
	static WSADATA	m_wsaData;
	static unsigned	m_instances;
	static const unsigned m_maxBuf;

	SOCKET			m_remoteSocket;
	SOCKET			m_socket;
	Kind			m_kind;

	sockaddr_in m_addrIn;
};


#endif // _SOCKET_HPP_