﻿//
// MainPage.xaml.cpp
// Implementation of the MainPage class.
//

#include "pch.h"
#include "MainPage.xaml.h"

#include <sstream>
#include <chrono>

using namespace RIClient;

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::UI::Text::Core;

using namespace Windows::System::Threading;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

MainPage::MainPage()
	: m_client(Socket::Kind::CLIENT)
{
	InitializeComponent();
}



void RIClient::MainPage::button_Click_1(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{

}


void RIClient::MainPage::HamburgerButton_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	splitView->IsPaneOpen = !splitView->IsPaneOpen;
}


void RIClient::MainPage::connectButton_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	connectGrid->Visibility = Windows::UI::Xaml::Visibility::Visible;
	keyboardGrid->Visibility = Windows::UI::Xaml::Visibility::Collapsed;
	mouseGrid->Visibility = Windows::UI::Xaml::Visibility::Collapsed;
}


void RIClient::MainPage::keyboardButton_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	connectGrid->Visibility = Windows::UI::Xaml::Visibility::Collapsed;
	keyboardGrid->Visibility = Windows::UI::Xaml::Visibility::Visible;
	mouseGrid->Visibility = Windows::UI::Xaml::Visibility::Collapsed;
}


void RIClient::MainPage::mouseButton_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	connectGrid->Visibility = Windows::UI::Xaml::Visibility::Collapsed;
	keyboardGrid->Visibility = Windows::UI::Xaml::Visibility::Collapsed;
	mouseGrid->Visibility = Windows::UI::Xaml::Visibility::Visible;
}


void RIClient::MainPage::button_Click_2(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	try
	{
		Platform::String^ ipRT = textBox->Text;
		std::wstring ipW(ipRT->Begin());
		std::string ipA(ipW.begin(), ipW.end());

		m_client.runClient(ipA.c_str());

		auto sendEventsHadler = ref new WorkItemHandler([this](IAsyncAction ^)
		{
			while (true)
			{
				sendEvents();
			}
		});

		ThreadPool::RunAsync(sendEventsHadler, WorkItemPriority::High);

	}
	catch (const std::exception& _e)
	{
		ContentDialog^ socketExceptionDialog = ref new ContentDialog();

		std::wstring ws;
		std::string s(_e.what());
		ws.assign(s.begin(), s.end());

		socketExceptionDialog->Title = "Socket excepation has being raised:";
		socketExceptionDialog->Content = ref new Platform::String(ws.c_str());
		socketExceptionDialog->PrimaryButtonText = "Ok";

		socketExceptionDialog->ShowAsync();
	}
}



void RIClient::MainPage::contentGrid_Tapped(Platform::Object^ sender, Windows::UI::Xaml::Input::TappedRoutedEventArgs^ e)
{

}


void RIClient::MainPage::mouseGrid_PointerMoved(Platform::Object^ sender, Windows::UI::Xaml::Input::PointerRoutedEventArgs^ e)
{
	auto point = e->GetCurrentPoint(mouseGrid);

	if (m_prevPoint == nullptr)
	{
		m_prevPoint = point;
		return;
	}

	auto deltaX = m_prevPoint->Position.X - point->Position.X;
	auto deltaY = m_prevPoint->Position.Y - point->Position.Y;
	
	std::stringstream eventStream;

	eventStream
		<< EventType::Mouse
		<< ' '
		<< static_cast<LONG>(deltaX * 10)
		<< ' '
		<< static_cast<LONG>(deltaY * 10)
		<< ' '
		<< NULL
		<< ' '
		<< MOUSEEVENTF_MOVE
	;

	m_events.push(eventStream.str());

	m_prevPoint = point;
}


void RIClient::MainPage::sendEvents()
{
	while (!m_events.empty())
	{
		m_client.write(m_events.front());
		m_events.pop();
	}
}


void RIClient::MainPage::mouseGrid_PointerEntered(Platform::Object^ sender, Windows::UI::Xaml::Input::PointerRoutedEventArgs^ e)
{

}


void RIClient::MainPage::mouseGrid_PointerExited(Platform::Object^ sender, Windows::UI::Xaml::Input::PointerRoutedEventArgs^ e)
{

}


void RIClient::MainPage::mouseGrid_PointerPressed(Platform::Object^ sender, Windows::UI::Xaml::Input::PointerRoutedEventArgs^ e)
{
	auto point = e->GetCurrentPoint(mouseGrid);

	if (m_prevPoint == nullptr)
	{
		m_prevPoint = point;
		return;
	}

	auto deltaX = m_prevPoint->Position.X - point->Position.X;
	auto deltaY = m_prevPoint->Position.Y - point->Position.Y;

	std::stringstream eventStream;

	eventStream
		<< EventType::Mouse
		<< ' '
		<< static_cast<LONG>(deltaX * 10)
		<< ' '
		<< static_cast<LONG>(deltaY * 10)
		<< ' '
		<< NULL
		<< ' '
		<< ( MOUSEEVENTF_LEFTDOWN )
		;

	m_events.push(eventStream.str());

	m_prevPoint = point;
}


void RIClient::MainPage::mouseGrid_PointerReleased(Platform::Object^ sender, Windows::UI::Xaml::Input::PointerRoutedEventArgs^ e)
{
	auto point = e->GetCurrentPoint(mouseGrid);

	if (m_prevPoint == nullptr)
	{
		m_prevPoint = point;
		return;
	}

	auto deltaX = m_prevPoint->Position.X - point->Position.X;
	auto deltaY = m_prevPoint->Position.Y - point->Position.Y;

	std::stringstream eventStream;

	eventStream
		<< EventType::Mouse
		<< ' '
		<< static_cast<LONG>(deltaX * 10)
		<< ' '
		<< static_cast<LONG>(deltaY * 10)
		<< ' '
		<< NULL
		<< ' '
		<< ( MOUSEEVENTF_LEFTUP )
		;

	m_events.push(eventStream.str());

	m_prevPoint = point;
}


void RIClient::MainPage::keyboardTextBox_TextChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e)
{
	//std::wstring newText = keyboardTextBox->Text->Data();

	//int i = 0;
	//for ( ; i < newText.size() && i < m_oldText.size(); ++i)
	//{
	//	if (newText[i] != m_oldText[i])
	//		break;
	//}

	//for (; i < newText.size(); ++i)
	//{
	//	std::stringstream eventStream;

	//	eventStream
	//		<< EventType::Keyboard
	//		<< ' '
	//		<< NULL
	//		<< ' '
	//		<< static_cast<DWORD>(newText[i])
	//		<< ' '
	//		<< 0x0004 // KEYEVENTF_UNICODE
	//		;

	//	m_events.push(eventStream.str());
	//}


	//m_oldText = newText;
}


void RIClient::MainPage::keyboardTextBox_KeyDown(Platform::Object^ sender, Windows::UI::Xaml::Input::KeyRoutedEventArgs^ e)
{
	std::stringstream eventStream;

	eventStream
		<< EventType::Keyboard
		<< ' '
		<< static_cast<DWORD>(e->Key)
		<< ' '
		<< NULL
		<< ' '
		<< 0
		;

	m_events.push(eventStream.str());
}


void RIClient::MainPage::keyboardTextBox_KeyUp(Platform::Object^ sender, Windows::UI::Xaml::Input::KeyRoutedEventArgs^ e)
{
	std::stringstream eventStream;

	eventStream
		<< EventType::Keyboard
		<< ' '
		<< static_cast<DWORD>(e->Key)
		<< ' '
		<< NULL
		<< ' '
		<< 0x0002 // KEYEVENTF_KEYUP
		;

	m_events.push(eventStream.str());
}
