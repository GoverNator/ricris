#include "Socket.hpp"
#include "..\CommonData.hpp"

#include <ws2tcpip.h>
#include <windows.h>

#include <stdexcept>
#include <thread>
#include <chrono>
#include <iostream>

#pragma comment (lib, "Ws2_32.lib")

WSADATA Socket::m_wsaData = WSADATA();
unsigned Socket::m_instances = 0;
const unsigned Socket::m_maxBuf = 100;

void Socket::initialize()
{
	const int result = WSAStartup(MAKEWORD(2, 2), &m_wsaData);

	if (result != 0) 
	{
		throw std::runtime_error("WSAStartup failed");
	}
}

Socket::Socket(Kind _kind)
	:	m_socket(INVALID_SOCKET)
	,	m_remoteSocket(INVALID_SOCKET)
	,	m_kind( _kind )
{
	if (m_instances++ == 0)
		initialize();
}

Socket::~Socket()
{
	reset();

	if (--m_instances == 0)
		WSACleanup();
}

void Socket::runServer(const char * _port)
{
	if (m_kind == Kind::CLIENT)
		throw std::runtime_error("it is CLIENT socket");

	if (strcmp(_port, "default") == 0)
		_port = DEFAULT_PORT;

	addrinfo *addrinfoResult = nullptr, hints;

	fillAddrInfo(nullptr, _port, &addrinfoResult, hints);

	prepareSocket(addrinfoResult);

	int result = bind(m_socket, addrinfoResult->ai_addr, (int)addrinfoResult->ai_addrlen);

	freeaddrinfo(addrinfoResult);

	if (result == SOCKET_ERROR)
		throw std::runtime_error("connect socket failed");

	result = listen(m_socket, SOMAXCONN);

	if ( result == SOCKET_ERROR)
		throw std::runtime_error("listen socket failed");
}

//TODO validate IP adress
void Socket::runClient(const char * _ip, const char * _port)
{
	if (m_kind == Kind::SERVER)
		throw std::runtime_error("it is SERVER socket");

	if (strcmp(_port, "default") == 0)
		_port = DEFAULT_PORT;

	addrinfo *addrinfoResult = nullptr, hints;

	fillAddrInfo(_ip, _port, &addrinfoResult, hints);

	int result = SOCKET_ERROR;

	for (addrinfo * ptr = addrinfoResult; ptr != NULL; ptr = ptr->ai_next)
	{
		prepareSocket(ptr);

		result = connect(m_socket, ptr->ai_addr, (int)ptr->ai_addrlen);

		freeaddrinfo(addrinfoResult);

		if (result == SOCKET_ERROR)
			continue;
	}

	if (result == SOCKET_ERROR)
		throw std::runtime_error("connect socket failed");
}

void Socket::reset()
{
	closesocket(m_socket);
	m_socket = INVALID_SOCKET;
}

std::string Socket::read()
{
	char recvbuf[m_maxBuf];

	const SOCKET & sock =
		(m_kind == Kind::CLIENT)
		? m_socket
		: m_remoteSocket
	;

	const int result = recv(sock, recvbuf, m_maxBuf, 0);

	return std::string(recvbuf);
}

void Socket::write(const std::string & _data)
{
	if (_data.size() > m_maxBuf - 1)
		throw std::runtime_error("too big msg");

	const SOCKET & sock
		= (m_kind == Kind::CLIENT)
		? m_socket
		: m_remoteSocket
	;

	const int result = send(sock, _data.c_str(), m_maxBuf, 0);
}

void Socket::getServerIP()
{
	if (m_kind == Kind::CLIENT)
		throw std::runtime_error("it is CLIENT socket");

	char hostName[80];

	if (gethostname(hostName, sizeof(hostName)) == SOCKET_ERROR) 
		throw std::runtime_error("failed to get server name");

	struct hostent *adresses = gethostbyname(hostName);
	if (adresses == nullptr) 
		throw std::runtime_error("failed to get server ip");

	for (int i = 0; adresses->h_addr_list[i] != nullptr; ++i)
	{
		struct in_addr addr;
		memcpy(&addr, adresses->h_addr_list[i], sizeof(struct in_addr));
		std::cout << "Address " << i << ": " << inet_ntoa(addr) << std::endl;
	}
}

void Socket::fillAddrInfo(
	const char * _ip
	, const char * _port
	, addrinfo ** _addrInfo
	, addrinfo & _hints
)
{
	ZeroMemory(&_hints, sizeof(_hints));

	_hints.ai_socktype = SOCK_STREAM;
	_hints.ai_protocol = IPPROTO_TCP;

	if (m_kind == Kind::SERVER)
	{
		_hints.ai_family = AF_INET;
		_hints.ai_flags = AI_PASSIVE;
	}
	else
	{
		_hints.ai_family = AF_UNSPEC;
	}

	if (getaddrinfo(_ip, _port, &_hints, _addrInfo) != 0)
		throw std::runtime_error("getaddrinfo failed");
}


void Socket::prepareSocket(addrinfo * _addrinfo)
{
	m_socket = socket(_addrinfo->ai_family, _addrinfo->ai_socktype, _addrinfo->ai_protocol);

	if (m_socket == INVALID_SOCKET)
		throw std::runtime_error("creating socket failed");
}

void Socket::acceptClient()
{
	if (m_kind == Kind::CLIENT)
		throw std::runtime_error("it is CLIENT socket");

	// Accept a client socket
	m_remoteSocket = accept(m_socket, NULL, NULL);

	if (m_remoteSocket == INVALID_SOCKET)
		throw std::runtime_error("accept failed");
}
